function blur (blur_px){
	var a='blur('+blur_px+'px)';
	$('.heady').css('filter',a);
	$('.main_content').css('filter',a);
	$('.footer').css('filter',a);
}
$('.do_order_button').click(function(){
	blur(5);
	$('.modal_form_do_order').fadeIn();
	// $('.modal_form_do_order').css('display', 'block');
})

$('.form_do_order_cross').click(function() { 
	blur(0);
	$('.modal_form_do_order').fadeOut();
});

$(document).mouseup(function (e) { 
	var popup = $('.modal_form_do_order');
	if (e.target!=popup[0]&&popup.has(e.target).length === 0){
		$('.modal_form_do_order').fadeOut();
		blur(0);
	}
});