var owl_1 = $('.owl-carousel');	
owl_1.owlCarousel({
	loop:true,
	margin:0,
	nav:false,
	items:1,
	dots: false,
	autoplay: true,
	autoplayTimeout: 11000,
	smartSpeed: 1600,
});
$('.custom_next_btn').click(function() {
	owl_1.trigger('next.owl.carousel',500);
});
$('.custom_previous_btn').click(function() {
	owl_1.trigger('prev.owl.carousel',500);
});